﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Missile : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] Vector2 assignedVelocity = Vector2.zero;
    [SerializeField] Rigidbody2D assignedRigidbody = null;
    #endregion

    #region METHODS
    void Awake()
    {
        if (assignedRigidbody == null) { assignedRigidbody = GetComponent<Rigidbody2D>(); }
        assignedRigidbody.useFullKinematicContacts = true;
    }

    void FixedUpdate()
    {
        var newPosition = assignedRigidbody.position + (assignedVelocity * Time.fixedDeltaTime);
        this.assignedRigidbody.MovePosition(newPosition);
    }
    #endregion
}
