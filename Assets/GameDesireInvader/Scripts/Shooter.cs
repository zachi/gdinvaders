﻿using UnityEngine;

public class Shooter : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] GameObject projectilePrefab = null;
    [SerializeField] float cooldownInSeconds = 0f;

    float LastShoot { get; set; } = 0f;
    bool CanPerform
    {
        get
        {
            return LastShoot + cooldownInSeconds < Time.time;
        }
    }
    #endregion

    #region METHODS
    void Start()
    {
        LastShoot = Time.time + cooldownInSeconds;
    }

    public bool RequestShoot()
    {
        if (this.CanPerform)
        {
            ShootProjectile();
            return true;
        }
        return false;
    }

    void ShootProjectile()
    {
        LastShoot = Time.time;
        var projectile = GameObject.Instantiate(projectilePrefab);
        projectile.transform.position = this.transform.position;
    }

    #endregion
}
