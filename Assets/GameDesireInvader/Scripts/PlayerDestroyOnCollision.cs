﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(Collider2D))]
public class PlayerDestroyOnCollision : MonoBehaviour
{
    public MonoBehaviour[] deactivateWhenDead = null;
    bool isDead = false;
    Vector3 originalScale = Vector3.zero;

    void Awake()
    {
        originalScale = this.transform.localScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDead) return;

        MessageBroker.Default.Publish(new PlayerDestroyedMessage(this));
        GameObject.Destroy(collision.gameObject);
        foreach(var c in deactivateWhenDead)
        {
            c.enabled = false;
        }
        if(Logic.Instance.gameState.CurrentLives.Value > 0)
        {
            Invoke(nameof(NextLive), 2f);
        }
        isDead = true;

        this.transform.localScale = Vector3.zero;
    }


    public void NextLive()
    {
        isDead = false;
        this.gameObject.SetActive(true);
        foreach (var c in deactivateWhenDead)
        {
            c.enabled = true;
        }

        this.transform.localScale = originalScale;
    }

}
