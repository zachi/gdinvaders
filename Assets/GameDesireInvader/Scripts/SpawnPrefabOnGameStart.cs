﻿using UnityEngine;
using UniRx;

public class SpawnPrefabOnGameStart : MonoBehaviour
{
    [SerializeField] EGameState[] OnState = null;
    [SerializeField] GameObject prefab = null;

    void Awake()
    {
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Where(p => System.Array.IndexOf(OnState, p.GameState) > -1).Subscribe(_ => SpawnPrefab()).AddTo(this);
    }

    void SpawnPrefab()
    {
        GameObject.Instantiate(prefab, this.transform);
    }
}
