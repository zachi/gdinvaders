﻿using UnityEngine;
using UniRx;

public class DestroyWhenOutOfBounds : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] Bounds bounds;
    #endregion

    #region METHODS
    void Awake()
    {
        this.transform.ObserveEveryValueChanged(y => y.position).Where(IsOutOfBounds).Subscribe(DestroyMe).AddTo(this);
    }

    bool IsOutOfBounds(Vector3 v)
    {
        return !bounds.Contains(v);
    }

    void DestroyMe(Vector3 v)
    {
        GameObject.Destroy(this.gameObject);
    }
    #endregion
}
