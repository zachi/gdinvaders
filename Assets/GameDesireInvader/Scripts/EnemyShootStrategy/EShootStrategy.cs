﻿public enum EShootStrategy
{
    None,
    AtRandom,
    AtProximity,
    Defensive
}

public static class _Ext_EShootStrategy
{
    public static IEnemyShootStrategy Materialize(this EShootStrategy type)
    {
        switch(type)
        {
            case EShootStrategy.None:
                return new NullShootStrategy();
            case EShootStrategy.AtRandom:
                return new AtRandomShootStrategy();
            case EShootStrategy.AtProximity:
                return new AtProximityShootStrategy();
            case EShootStrategy.Defensive:
                return new DefensiveShootStrategy();

        }

        return new NullShootStrategy();

    }
}
