﻿using System;
using UnityEngine;

public interface IEnemyShootStrategy
{
    EShootStrategy MyShootStrategy { get; }
    event Action tryToShoot;
    void UpdateShootStrategy(Vector2 myPosition, Vector2 playerPosition);
}
