﻿using System;
using UnityEngine;

public class DefensiveShootStrategy : IEnemyShootStrategy
{
    const string PLAYER_MISSILE_LAYER = "PlayerProjectile";
    public EShootStrategy MyShootStrategy => EShootStrategy.Defensive;

    public event Action tryToShoot;

    public void UpdateShootStrategy(Vector2 myPosition, Vector2 playerPosition)
    {
        var result = Physics2D.CircleCast(myPosition, 1f, Vector2.down, 3f, LayerMask.NameToLayer(PLAYER_MISSILE_LAYER));
        if (result.collider)
        {
            tryToShoot?.Invoke();
        }
    }
}
