﻿using System;
using UnityEngine;

public class NullShootStrategy : IEnemyShootStrategy
{
    public EShootStrategy MyShootStrategy { get { return EShootStrategy.None; } }

    public event Action tryToShoot;

    public void UpdateShootStrategy(Vector2 myPosition, Vector2 playerPosition)
    {
        // NO OP
    }
}
