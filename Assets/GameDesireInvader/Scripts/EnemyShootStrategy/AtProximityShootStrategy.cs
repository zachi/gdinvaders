﻿using System;
using UnityEngine;

public class AtProximityShootStrategy : IEnemyShootStrategy
{
    const float SHOOT_ON_PROXIMITY = 1f;

    public EShootStrategy MyShootStrategy => EShootStrategy.AtProximity;

    public event Action tryToShoot;

    public void UpdateShootStrategy(Vector2 myPosition, Vector2 playerPosition)
    {
        if (Mathf.Abs(myPosition.x - playerPosition.x) < SHOOT_ON_PROXIMITY)
            tryToShoot?.Invoke();
    }
}
