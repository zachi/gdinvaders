﻿using System;
using UnityEngine;

public class AtRandomShootStrategy : IEnemyShootStrategy
{
    const float SHOOT_PROBABILITY_PER_LOGICAL_FRAME = .1f;

    public EShootStrategy MyShootStrategy => EShootStrategy.AtRandom;

    public event Action tryToShoot;

    public void UpdateShootStrategy(Vector2 myPosition, Vector2 playerPosition)
    {
        if (UnityEngine.Random.Range(0f, 1f) < SHOOT_PROBABILITY_PER_LOGICAL_FRAME)
        {
            tryToShoot?.Invoke();
        }
    }
}
