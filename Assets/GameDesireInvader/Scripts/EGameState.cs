﻿public enum EGameState
{
    Menu,
    Start,
    Ongoing,
    Pause,
    NextLevel,
    GameOver
}
