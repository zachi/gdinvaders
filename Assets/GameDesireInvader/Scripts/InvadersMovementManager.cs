﻿using System;
using UnityEngine;
using UniRx;

public class InvadersMovementManager : MonoBehaviour
{
    #region MEMBERS
    public static InvadersMovementManager Instance { get; private set; }

    [Header("Config")]
    [SerializeField] public FloatReactiveProperty MaxDistanceFromCenter = new FloatReactiveProperty(3f);
    [SerializeField] FloatReactiveProperty BaseSpeed = new FloatReactiveProperty(1f);

    [Tooltip("true is right, false is left")]
    [SerializeField] BoolReactiveProperty StartDirection = new BoolReactiveProperty(true);
    public BoolReactiveProperty CurrentDirection = new BoolReactiveProperty();
    public BoolReactiveProperty CurrentDirectionNF = new BoolReactiveProperty();
    [SerializeField] float dontGoUnderThisLine = -1.5f;

    public ReadOnlyReactiveProperty<float> CurrentSpeed { get; private set; }

    ReactiveCollection<InvadersMovement> Invaders = new ReactiveCollection<InvadersMovement>();


    #endregion

    #region METHODS
    void Awake()
    {
        InvadersMovementManager.Instance = this;
        CurrentDirection.Value = StartDirection.Value;
        CurrentSpeed = CurrentDirection.Select(_ => _ ? 1f : -1f).CombineLatest(BaseSpeed, (f1, f2) => { return f1 * f2; }).ToReadOnlyReactiveProperty().AddTo(this);
        MessageBroker.Default.Receive<CheatCommand>().Where(c => c == CheatCommand.DESTROY_MOVEMENT_MANAGER).Subscribe(_ => GameObject.Destroy(this.gameObject)).AddTo(this);
        MessageBroker.Default.Receive<EnemyDestroyedMessage>().Subscribe(Faster).AddTo(this);
        Invaders.ObserveCountChanged().ThrottleFrame(2).Where(_ => _ == 0).Subscribe(OnAllInvadersKilled).AddTo(this);
        CurrentDirection.Pairwise().Subscribe(MoveDownRow).AddTo(this);
    }

    void Update()
    {
        this.transform.position += (Vector3)(Vector2.right * this.CurrentSpeed.Value * Time.deltaTime);
    }

    void LateUpdate()
    {
        this.CurrentDirection.Value = this.CurrentDirectionNF.Value;

    }

    void Faster(EnemyDestroyedMessage msg)
    {
        this.BaseSpeed.Value = this.BaseSpeed.Value * 1.035f;
    }

    void MoveDownRow(Pair<bool> p)
    {
        if (Invaders.Count == 0)
            return;

        if (p.Previous != p.Current)
        {
            var min = Invaders[0].transform.position.y;
            foreach(var invader in Invaders)
            {
                min = Mathf.Min(min, invader.transform.position.y);
            }

            if(min > dontGoUnderThisLine)
                this.transform.localPosition += Vector3.down * .4f;

        }
    }

    public void RegisterInvader(InvadersMovement invader)
    {
        Invaders.Add(invader);
    }

    public void UnregisterInvader(InvadersMovement invader)
    {
        Invaders.Remove(invader);
        if(Invaders.Count == 0)
        {
            InvadersMovementManager.Instance = null;
            GameObject.Destroy(this.gameObject, 2.5f);
        }
    }

    void OnAllInvadersKilled(int count = 0)
    {
        Debug.Log("ALL KILLED");
        MessageBroker.Default.Publish(new AllEnemiesDestroyedMessage());
    }

    #endregion
}
