﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class DestroyObjectOnGameState : MonoBehaviour
{
    [SerializeField] EGameState[] OnState = null;

    private void Awake()
    {
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Where(p => System.Array.IndexOf(OnState, p.GameState) > -1).Subscribe(_ => DestroyMe()).AddTo(this);
    }

    void DestroyMe()
    {
        GameObject.Destroy(this.gameObject);
    }
}
