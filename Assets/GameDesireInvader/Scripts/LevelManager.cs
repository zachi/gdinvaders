﻿using UnityEngine;
using UnityEngine.Assertions;
using UniRx;
using AnimPlugin;

public class LevelManager : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] InvadersMovementManager[] levels = null;
    int CurrentLevelIndex = 0;
    #endregion

    private void Awake()
    {
        Assert.IsNotNull(levels);
        Assert.IsFalse(levels.Length == 0, "Levels not bind to LevelManager");

        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Subscribe(OnGameStateChanged).AddTo(this);
    }

    void SpawnLevel()
    {
        GameObject.Instantiate(levels[CurrentLevelIndex++], this.transform);
        Logic.Instance.ChangeGameStateTo(EGameState.Ongoing);
        if (CurrentLevelIndex >= levels.Length)
            CurrentLevelIndex = 0;
    }

    void Intermission()
    {
        StartCoroutine(
            Anim.Chain(
                Anim.Wait(2.4f),
                Anim.Void(SpawnLevel)
                )
            );
    }

    void OnGameStateChanged(ChangeGameStateRequestMessage message)
    {
        switch (message.GameState)
        {
            case EGameState.Start:
                CurrentLevelIndex = 0;
                SpawnLevel();
                break;
            case EGameState.NextLevel:
                Intermission();
                break;
        }
    }
}
