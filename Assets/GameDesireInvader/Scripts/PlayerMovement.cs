﻿using UnityEngine;
using UniRx;

[RequireComponent(typeof(Shooter))]
public class PlayerMovement : MonoBehaviour
{
    #region CONSTS
    const string AXIS_NAME = "Horizontal";
    const string FIRE_NAME = "Fire";
    #endregion

    #region MEMBERS
    public static Vector3 GlobalAccessToPlayerPosition { get; private set; }

    [SerializeField] FloatReactiveProperty DefaultSpeedMultiply = new FloatReactiveProperty(1f);
    [SerializeField] FloatReactiveProperty SpeedBonus = new FloatReactiveProperty(0f);
    [SerializeField] FloatReactiveProperty MaxDistanceFromCenter = new FloatReactiveProperty(3f);

    ReadOnlyReactiveProperty<float> CurrentSpeedMultiply;
    Shooter MyShooter { get; set; }
    #endregion

    #region METHODS
    private void Awake()
    {
        if (MyShooter == null)
        {
            MyShooter = GetComponent<Shooter>();
        }
    }

    void Start()
    {
        CurrentSpeedMultiply =
            DefaultSpeedMultiply.CombineLatest(SpeedBonus, (x, y) => { return x * (1 + y); })
            .ToReadOnlyReactiveProperty()
            .AddTo(this);
    }

    void Update()
    {
        var changeXTo = this.transform.localPosition.x + CurrentSpeedMultiply.Value * Time.deltaTime * Input.GetAxis(AXIS_NAME);
        changeXTo = Mathf.Clamp(changeXTo, -MaxDistanceFromCenter.Value, MaxDistanceFromCenter.Value);

        this.transform.localPosition = new Vector3(changeXTo, this.transform.localPosition.y, this.transform.localPosition.z);

        if (Input.GetButton(FIRE_NAME))
        {
            MyShooter.RequestShoot();
        }
    }

    void LateUpdate()
    {
        GlobalAccessToPlayerPosition = this.transform.position;
    }
    #endregion
}
