﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGameStateOnFire : MonoBehaviour
{
    const string BTN_CHOOSE = "Fire";

    [SerializeField] EGameState targetGameState;

    void Update()
    {
        if (Input.GetButtonDown(BTN_CHOOSE))
        {
            Logic.Instance.ChangeGameStateTo(targetGameState);
        }
    }
}
