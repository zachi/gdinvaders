﻿using UnityEngine;
using UniRx;

public class Logic : MonoBehaviour
{
    #region MEMBERS
    public static Logic Instance;
    public GameState gameState = new GameState();
    public EGameState CurrentState { get; protected set; } = EGameState.Menu;
    #endregion

    #region METHODS
    public void ChangeGameStateTo(EGameState gameState)
    {
        if(gameState != CurrentState)
        {
            CurrentState = gameState;
            MessageBroker.Default.Publish(new ChangeGameStateRequestMessage(CurrentState, gameState));
        }
    }

    void Awake()
    {
        Instance = this;
    }


    void Start()
    {
        MessageBroker.Default.Publish(new ChangeGameStateRequestMessage(EGameState.Menu, EGameState.Menu));
    }
    #endregion
}
