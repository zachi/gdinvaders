﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(DisplayLabelInt))]
public class DisplayLevel : MonoBehaviour
{
    #region MEMBERS
    DisplayLabelInt MyLabel { get; set; }
    #endregion

    private void Awake()
    {
        if (MyLabel == null) { MyLabel = GetComponent<DisplayLabelInt>(); }
    }

    void Start()
    {
        SetupGameState(Logic.Instance.gameState);
    }

    void SetupGameState(GameState gs)
    {
        MyLabel.Bind(gs.CurrentLevel);
    }
}
