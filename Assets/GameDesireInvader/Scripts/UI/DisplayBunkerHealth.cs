﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class DisplayBunkerHealth : MonoBehaviour
{
    [SerializeField] CanBeDestroyed bunker = null;
    [SerializeField] Image resultImage = null;

    void Awake()
    {
        bunker.CurrentHitPoints.Subscribe(ShowResult).AddTo(this);
    }

    void ShowResult(float f)
    {
        resultImage.fillAmount = f / bunker.maxHitPoints;
    }
}
