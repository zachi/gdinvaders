﻿using UnityEngine;
using UniRx;

[RequireComponent(typeof(DisplayLabelInt))]
public class DisplayScores : MonoBehaviour
{
    #region MEMBERS
    DisplayLabelInt MyLabel { get; set; }
    #endregion

    private void Awake()
    {
        if (MyLabel == null) { MyLabel = GetComponent<DisplayLabelInt>(); }
    }

    void Start()
    {
        SetupGameState(Logic.Instance.gameState);
    }

    void SetupGameState(GameState gs)
    {
        MyLabel.Bind(gs.CurrentPoints);
    }
}
