﻿using UnityEngine;
using TMPro;
using UniRx;

public class DisplayLabelInt : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] TextMeshProUGUI label = null;
    [SerializeField] string LabelFormat = "Scores: {0}";
    #endregion

    #region METHODS
    public void Bind(IntReactiveProperty rproperty)
    {
        rproperty.Subscribe(OnValChanged).AddTo(this);
    }

    void OnValChanged(int val)
    {
        label.text = string.Format(LabelFormat, val);
    }
    #endregion
}
