﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using TMPro;
using UnityEngine.Events;

[RequireComponent(typeof(TextMeshProUGUI))]
public class FakeButton : MonoBehaviour
{
    #region CONSTS
    const string FORMAT_SELECTED_DECORATE = ">> {0} <<";
    const float SCALE_WHEN_SELECTED = 1.4f;
    const float SCALE_WHEN_NOT_SELECTED = 1f;
    #endregion

    #region MEMBERS
    [SerializeField] public int order = 0;
    [SerializeField] TextMeshProUGUI label = null;
    [SerializeField] string DefaultLabelString = null;
    [SerializeField] MenuContainer myMenuContainer = null;
    [SerializeField] UnityEvent action = null;

    public BoolReactiveProperty Selected = new BoolReactiveProperty(false);
    #endregion

    #region MEMBERS
    public void Fire()
    {
        action?.Invoke();
    }

    void Awake()
    {
        if (label == null) { label = this.GetComponent<TextMeshProUGUI>(); }
    }

    void Start()
    {
        this.myMenuContainer.RegisterButton(this);
        this.Selected.Subscribe(OnSelectionChanged).AddTo(this);
    }

    void OnSelectionChanged(bool isSelected)
    {
        this.transform.localScale = isSelected ?
            (Vector3.one * SCALE_WHEN_SELECTED) :
            (Vector3.one * SCALE_WHEN_NOT_SELECTED);

        this.label.text = isSelected ?
            string.Format(FORMAT_SELECTED_DECORATE, DefaultLabelString) :
            DefaultLabelString;
    }

    #endregion
}

