﻿using AnimPlugin;
using UnityEngine;
using UniRx;

public class HideOnGame : MonoBehaviour
{
    [SerializeField] CanvasGroup canvasGroup = null;
    bool isEnabled = false;

    void Awake()
    {
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Subscribe(OnGameStateChanged).AddTo(this);
    }

    void OnGameStateChanged(ChangeGameStateRequestMessage message)
    {
        switch (message.GameState)
        {
            case EGameState.Menu:
                EnableMenu();
                break;
            default:
                DisableMenu();
                break;
        }
    }

    void EnableMenu()
    {
        if (isEnabled) return;
        this.StopAllCoroutines();
        isEnabled = true;
        SetTransparency01(0f);
        this.transform.localScale = Vector3.one * 3f;

        this.StartCoroutine(
            Anim.Parallel(
                Anim.PerformFunction(SetTransparency01, 0f, 1f, .6f),
                Anim.ScaleTo(this.transform, Vector3.one * 1f, .33f, Ease.QuintOut)
            )
        );
    }

    void DisableMenu()
    {
        if (!isEnabled) return;
        isEnabled = false;
        SetTransparency01(1f);
        this.transform.localScale = Vector3.one * 1f;

        this.StartCoroutine(
            Anim.Parallel(
                Anim.PerformFunction(SetTransparency01, 1f, 0f, .33f),
                Anim.ScaleTo(this.transform, Vector3.one * 1.4f, .33f, Ease.QuintOut)
            )
        );
    }

    void SetTransparency01(float transparency)
    {
        this.canvasGroup.alpha = transparency;
    }
}
