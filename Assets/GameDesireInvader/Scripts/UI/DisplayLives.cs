﻿using UnityEngine;
using UniRx;

public class DisplayLives : MonoBehaviour
{
    [SerializeField] RectTransform singleLivePrefab = null;
    [SerializeField] Transform container = null;

    private void Start()
    {
        Bind(Logic.Instance.gameState.CurrentLives);
    }

    void Bind(IntReactiveProperty livesCounter)
    {
        Clear();

        livesCounter.First().Subscribe(CreateLivesOnInit).AddTo(this);
        livesCounter.Pairwise().Subscribe(OnLivesCountChanged).AddTo(this);
    }

    void CreateLivesOnInit(int lives)
    {
        for (int i = 0; i < lives; i++)
        {
            GameObject.Instantiate(singleLivePrefab, container);
        }
    }

    void OnLivesCountChanged(Pair<int> change)
    {
        if (change.Current < change.Previous)
        {
            var r = change.Previous - change.Current;
            for (int i = 0; i < r; i++)
                GameObject.Destroy(container.GetChild(0).gameObject);
        }
        else
        {
            var r = change.Current - change.Previous;
            for (int i = 0; i < r; i++)
                GameObject.Instantiate(singleLivePrefab, container);
        }
    }

    void Clear()
    {
        var count = container.childCount;
        for (int i = count - 1; i >= 0; i--)
        {
            GameObject.Destroy(container.GetChild(i).gameObject);
        }
    }
}
