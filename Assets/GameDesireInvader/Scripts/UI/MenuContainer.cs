﻿using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class MenuContainer : MonoBehaviour, IComparer<FakeButton>
{
    const string BTN_NEXT_MENU = "Next_Menu_Item";
    const string BTN_PREV_MENU = "Prev_Menu_Item";
    const string BTN_CHOOSE = "Fire";

    List<FakeButton> registeredButtons = new List<FakeButton>();
    bool isDirty = false;
    int currentSelectionIndex = 0;
    bool ActionsEnabled = true;

    void Awake()
    {
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Subscribe(GameStateChanged).AddTo(this);
    }

    void GameStateChanged(ChangeGameStateRequestMessage message)
    {
        switch(message.GameState)
        {
            case EGameState.Menu:
                ActionsEnabled = true;
                break;
            default:
                ActionsEnabled = false;
                break;
        }
    }

    public void Action_QuitGame()
    {
        if (!ActionsEnabled)
            return;

        Application.Quit(0);
    }

    public void Action_StartGame()
    {
        if (!ActionsEnabled)
            return;
        Logic.Instance.ChangeGameStateTo(EGameState.Start);
    }

    public void RegisterButton(FakeButton button)
    {
        registeredButtons.Add(button);
        isDirty = true;
    }

    void Update()
    {
        var lastFrameSelected = currentSelectionIndex;
        if (Input.GetButtonDown(BTN_NEXT_MENU))
        {
            currentSelectionIndex++;
        }
        else if (Input.GetButtonDown(BTN_PREV_MENU))
        {
            currentSelectionIndex--;
            if(currentSelectionIndex < 0)
            {
                currentSelectionIndex = registeredButtons.Count - 1;
            }
        }

        currentSelectionIndex %= registeredButtons.Count;

        if (lastFrameSelected != currentSelectionIndex)
        {
            registeredButtons[lastFrameSelected].Selected.Value = false;
            registeredButtons[currentSelectionIndex].Selected.Value = true;
        }

        if (Input.GetButtonDown(BTN_CHOOSE))
        {
            registeredButtons[currentSelectionIndex].Fire();
        }
    }

    private void LateUpdate()
    {
        if (isDirty) ButtonsCountChanged();
    }

    void ButtonsCountChanged()
    {
        registeredButtons.Sort(this);
        if (currentSelectionIndex > this.registeredButtons.Count)
        {
            currentSelectionIndex = 0;
        }

        for (int i = 0; i < registeredButtons.Count; i++)
        {
            registeredButtons[i].Selected.Value = i == currentSelectionIndex;
        }
    }

    public int Compare(FakeButton x, FakeButton y)
    {
        return x.order - y.order;
    }
}
