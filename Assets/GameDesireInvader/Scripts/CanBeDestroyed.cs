﻿using UnityEngine;
using UniRx;

[RequireComponent(typeof(Collider2D))]
public class CanBeDestroyed : MonoBehaviour
{
    #region MEMBERS
    [SerializeField] public float maxHitPoints = 1f;
    [SerializeField] public FloatReactiveProperty CurrentHitPoints = new FloatReactiveProperty();
    [SerializeField] int AddPointsOnDestroy = 3;
    #endregion

    #region METHODS
    private void Start()
    {
        this.CurrentHitPoints.Value = maxHitPoints;
        this.CurrentHitPoints.Where(_ => _ == 0).Subscribe(_ => DestroyMe()).AddTo(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.CurrentHitPoints.Value -= 1f;
        GameObject.Destroy(collision.gameObject);
    }

    public void DestroyByCheat()
    {
        DestroyMe();
    }

    void DestroyMe()
    {
        if (this.AddPointsOnDestroy != 0)
            MessageBroker.Default.Publish(new EnemyDestroyedMessage(this.AddPointsOnDestroy));

        GameObject.Destroy(this.gameObject);
    }
    #endregion
}
