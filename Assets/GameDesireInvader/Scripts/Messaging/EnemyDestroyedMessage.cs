﻿public struct EnemyDestroyedMessage
{
    public readonly int EnemyPoints;

    public EnemyDestroyedMessage(int p)
    {
        this.EnemyPoints = p;
    }
}
