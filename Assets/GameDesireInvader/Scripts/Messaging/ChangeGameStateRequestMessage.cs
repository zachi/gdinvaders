﻿public struct ChangeGameStateRequestMessage
{
    public EGameState Previous;
    public EGameState GameState;

    public ChangeGameStateRequestMessage(EGameState fromState, EGameState toState)
    {
        this.Previous = fromState;
        this.GameState = toState;
    }
}
