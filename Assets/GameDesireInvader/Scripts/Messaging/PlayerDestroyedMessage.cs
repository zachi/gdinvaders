﻿public struct PlayerDestroyedMessage
{
    public PlayerDestroyOnCollision playerObject;
    public PlayerDestroyedMessage(PlayerDestroyOnCollision player)
    {
        playerObject = player;
    }

}
