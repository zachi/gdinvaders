﻿using UniRx;

public class GameState
{
    public IntReactiveProperty CurrentLevel = new IntReactiveProperty();
    public IntReactiveProperty CurrentLives = new IntReactiveProperty();
    public IntReactiveProperty CurrentPoints = new IntReactiveProperty();

    readonly CompositeDisposable disposables = new CompositeDisposable();

    public GameState()
    {
        MessageBroker.Default.Receive<EnemyDestroyedMessage>().Subscribe(OnEnemyDestroyed).AddTo(disposables);
        MessageBroker.Default.Receive<PlayerDestroyedMessage>().Subscribe(OnPlayerDestroyedMessage).AddTo(disposables);
        MessageBroker.Default.Receive<AllEnemiesDestroyedMessage>().Subscribe(OnAllEnemiesDestroyed).AddTo(disposables);
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Subscribe(OnStateChange).AddTo(disposables);
    }

    void OnEnemyDestroyed(EnemyDestroyedMessage msg)
    {
        this.CurrentPoints.Value += msg.EnemyPoints;
    }

    void OnPlayerDestroyedMessage(PlayerDestroyedMessage msg)
    {
        this.CurrentLives.Value--;
        if (CurrentLives.Value == 0)
            Logic.Instance.ChangeGameStateTo(EGameState.GameOver);
    }

    void OnAllEnemiesDestroyed(AllEnemiesDestroyedMessage msg)
    {
        this.CurrentLevel.Value++;
        Logic.Instance.ChangeGameStateTo(EGameState.NextLevel);
    }

    void OnStateChange(ChangeGameStateRequestMessage msg)
    {
        if (msg.GameState == EGameState.Start)
        {
            CurrentLives.Value = 3;
            CurrentLevel.Value = 1;
            CurrentPoints.Value = 0;
        }
    }
}
