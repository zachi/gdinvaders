﻿using UnityEngine;
using UniRx;

public class InvadersMovement : MonoBehaviour
{
    #region METHODS
    void Start()
    {
        InvadersMovementManager.Instance.RegisterInvader(this);

        MessageBroker.Default.Receive<CheatCommand>().Where(c => c == CheatCommand.KILL_ALL).Subscribe(_ => GameObject.Destroy(this.gameObject)).AddTo(this);
    }

    void Update()
    {
        if (this.transform.position.x > InvadersMovementManager.Instance.MaxDistanceFromCenter.Value)
        {
            SetTargetDirection(false);
        }
        else if (this.transform.position.x < -InvadersMovementManager.Instance.MaxDistanceFromCenter.Value)
        {
            SetTargetDirection(true);
        }
    }

    void SetTargetDirection(bool b)
    {
        InvadersMovementManager.Instance.CurrentDirectionNF.Value = b;
    }

    void OnDestroy()
    {
        InvadersMovementManager.Instance.UnregisterInvader(this);
    }
    #endregion
}
