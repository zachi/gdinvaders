﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class Cheats : MonoBehaviour
{
    public void Click_KillAll()
    {
        MessageBroker.Default.Publish(CheatCommand.KILL_ALL);
    }

    public void Click_DestroyMovementManager()
    {
        MessageBroker.Default.Publish(CheatCommand.DESTROY_MOVEMENT_MANAGER);
    }
}

public enum CheatCommand
{
    KILL_ALL,
    DESTROY_MOVEMENT_MANAGER,

}
