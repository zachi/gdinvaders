﻿using UnityEngine;

[RequireComponent(typeof(Shooter))]
public class InvaderShooter : MonoBehaviour
{
    #region MEMBERS
#if UNITY_EDITOR
    [ContextMenuItem("TestCanShoot", "Dbg_TestCanShoot")]
#endif
    [SerializeField] LayerMask ShootBlockers = new LayerMask();
    [SerializeField] float CircleRadius = 0f;
    [SerializeField] bool DebugDraw = false;
    [SerializeField] EShootStrategy myShootStrategyType = EShootStrategy.None;
    [SerializeField] float CheckShootFreq = 2f;

    IEnemyShootStrategy MyShootStrategy { get; set; } = null;
    Shooter MyShooter { get; set; } = null;
    #endregion

    #region METHODS

    private void Awake()
    {
        MyShooter = GetComponent<Shooter>();
        MyShootStrategy = myShootStrategyType.Materialize();
        MyShootStrategy.tryToShoot += TryToShoot;
    }

    private void OnDestroy()
    {
        MyShootStrategy.tryToShoot -= TryToShoot;
    }

    void TryToShoot()
    {
        MyShooter.RequestShoot();
    }

    private void Start()
    {
        InvokeRepeating("TryPerformShoot", CheckShootFreq, CheckShootFreq);
    }

    void TryPerformShoot()
    {
        if (CanShoot())
        {
            MyShootStrategy.UpdateShootStrategy(this.transform.position, PlayerMovement.GlobalAccessToPlayerPosition);
        }
    }

    bool CanShoot()
    {
        var result = Physics2D.CircleCast((Vector2)this.transform.position + Vector2.down * (.5f + 2 * CircleRadius), CircleRadius, Vector2.down, 10f, ShootBlockers);
        if (result.collider != null)
        {
            return false;
        }

        return true;
    }

    #if UNITY_EDITOR
    void Dbg_TestCanShoot()
    {
        Debug.Log($"{nameof(CanShoot)} returns {CanShoot()}");
    }

    private void OnDrawGizmos()
    {
        if (DebugDraw)
        {
            Gizmos.DrawLine(this.transform.position + Vector3.down * .5f + Vector3.left * (.5f * CircleRadius), this.transform.position + Vector3.down * 10f + Vector3.left * .5f * CircleRadius); ;
            Gizmos.DrawLine(this.transform.position + Vector3.down * .5f + Vector3.right * (.5f * CircleRadius), this.transform.position + Vector3.down * 10f + Vector3.right * .5f * CircleRadius);
            Gizmos.DrawLine(this.transform.position + Vector3.down * .5f, this.transform.position + Vector3.down * 10f);
        }
    }
    #endif

    #endregion
}
