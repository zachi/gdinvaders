﻿using UnityEngine;
using UniRx;

public class GameStateLogger : MonoBehaviour
{
    void Start()
    {
        MessageBroker.Default.Receive<ChangeGameStateRequestMessage>().Subscribe(LogGameStateChagned).AddTo(this);
    }

    void LogGameStateChagned(ChangeGameStateRequestMessage message)
    {
        Debug.Log($"Change game state to {message.GameState}");
    }
}
